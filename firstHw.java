
public class Main {
	
	public static void main(String[] args) {
	
	//------------------------------------Ders 6 DEĞİŞKENLER-------------------------------------------
		
		System.out.println("-----Ders 6 DEĞİŞKENLER");
		
		//System.out.println("Öğrenci sayım : 5");
		//System.out.println("Öğrenci sayım : 5");
		//System.out.println("Öğrenci sayım : 5");
		//System.out.println("Öğrenci sayım : 5");   YERİNE: ---Bellekte tasarruf amaçlı
		
	//REUSEABILITY:	
		int ogrenciSayisi = 8;
		String mesaj = "Öğrenci sayısı: ";
		System.out.println(mesaj + ogrenciSayisi);
		
	//------------------------------------Ders 7 TEMEL VERİ TİPLERİ-------------------------------------------
		
		System.out.println("-----Ders 7 TEMEL VERİ TİPLERİ");
		
		int sayi = 12;
		sayi = 13;
		double sayi2 = 0.8;
		
		//Java'daki Veri Tipleri:
		//1.Primitive Types:
		//true ya da false çalıştırır -boolean
		//tek karakter tanımlamak için, tek tırnakla ('') kullanılır -char
		//integer olanlar (tam sayılar) -byte(-128 +127 aralığı) -short(yaklaşık +-5 basamak) -int(yaklaşık +-10 basamak) -long(yaklaşık +-19 basamak) ---sırasıyla daha fazla bellek kullanılır
		//ondalıklı sayılar için -float
		//yine ondalıklı sayılar için ama daha kapsamlısı -double
		//2.Nonprimitive Types:
		//karakter topluluğu için -string
		//3.Null
		
	//------------------------------------Ders 8- CONDITIONALS------------------------------------------
		
		System.out.println("-----Ders 8- CONDITIONALS");
		
		int sayi3 = 20;
		if (sayi3<20) {
				System.out.println("Sayı 20'den küçüktür.");
		}else if (sayi3==20){
			System.out.println("Sayı 20'ye eşittir.");
		}else {
			System.out.println("Sayı 20'den büyüktür.");
		}
		//Defensive programming, veri kaçağını engellemek, her durumu deeğrlendirmek.
	
	//------------------------------------Ders 9- A PROJECT, EN BÜYÜK SAYIYI BULMA------------------------------------------
		
		System.out.println("-----Ders 9- A PROJECT, EN BÜYÜK SAYIYI BULMA");
		
		int sayi4= 20;
		int sayi5= 25;
		int sayi6= 2;
		int enBuyuk= sayi4;
		
		if(enBuyuk<sayi5) {
			enBuyuk = sayi5;
		}
		if(enBuyuk<sayi6) {
			enBuyuk = sayi6;
		}
		System.out.println("En büyük sayı "+enBuyuk);
		
	//------------------------------------Ders 10- SWITCH (ŞARTLI DALLANDIRMA) BLOCKS------------------------------------------
		
		System.out.println("-----Ders 10- SWITCH (ŞARTLI DALLANDIRMA) BLOCKS");
		
		char grade = 'B';
		
		switch(grade) {
			case 'A' :
				System.out.println("Mükemmel geçtiniz!");
				break;
			case 'B' :
				// B ve C durumu için aynı çıktıyı vermek istersek arası boş kalabiliir.
			case 'C' :
				System.out.println("İyi geçtiniz!");
				break;
			case 'D' :
				System.out.println("Geçtiniz!");
				break;
			case 'F' :
				System.out.println("Kaldınız.");
				break;
				default:
					System.out.println("Geçersiz not");
		}
		
	//------------------------------------Ders 11- LOOP(DÖNGÜLER)/FOR------------------------------------------
		
		System.out.println("-----Ders 11- LOOP(DÖNGÜLER)/FOR");
		
		for(int i=1; i<10; i++) {
			// for (sayaç = başlangıç değeri; şart; kaçar artacağı)
			// şart olarak i<=10 deseydim 10 dahil edilirdi - küçük eşittir ifadesi
			// i++ ve i = i+1 aynı şeydir, 1 eksiltmek için de i-- denebilir.
			// i+=2 deseydim 2şer artardı
			System.out.println(i);
			//Bu döngü 10 olana kadar (şart) döngü çalışmaya devam edecek.
		}
		System.out.println("For döngüsü bitti");
		
	//------------------------------------Ders 12- LOOP(DÖNGÜLER)/WHILE------------------------------------------
	
		System.out.println("-----Ders 12- LOOP(DÖNGÜLER)/WHILE");
		
		int i = 3;
		while(i<10) {
			System.out.println(i);
			i+=3;
		//sayaç; while(şart); print; kaçar artacağı;
		}
		
		System.out.println("While döngüsü bitti");

	//------------------------------------Ders 13- LOOP(DÖNGÜLER)/DO-WHILE------------------------------------------
		
		System.out.println("-----Ders 13- LOOP(DÖNGÜLER)/DO-WHILE");
		
		int j = 1;
		do {
			System.out.println(j);
			j+=3;
		}while(j<10);
		System.out.println("Do-while döngüsü bitti");
		
		//While döngüsüyle arasındaki farkı: Şart uuymasa bile Do-while çalışacak; önce do komutu gerçekleşecek.
		//Gerçek hayatta; Veri tabanına log (döngüye girildiğine veya döngünün çalıştığına dair) atabilmek için
		
		//:::: Örnek:
		
		int k = 100;
		do {
			System.out.println("Loglandı");
			if(k<10){
			System.out.println(k);
			k+=3;}
		}while(k<10);
		System.out.println("Do-while döngüsü bitti");
		
	//------------------------------------Ders 14- ARRAYS (DİZİLER)------------------------------------------
		
		System.out.println("-----Ders 14- ARRAYS (DİZİLER)");
		
		String ogrenci1 = "Engin";
		String ogrenci2 = "Ceren";
		String ogrenci3 = "Reyhan";
		
		System.out.println(ogrenci1);
		System.out.println(ogrenci2);
		System.out.println(ogrenci3);
		
		//Hepsini tekrar yapmaktansa veya yeni ekleneceği zaman uğraşılacağına:
		
		System.out.println("Daha iyi bir yöntemle yazılan: ");
		String[] ogrenciler = new String[3];
		ogrenciler[0]= "Engin";
		ogrenciler[1]= "Ceren";
		ogrenciler[2]= "Reyhan";
		
		for(int m=0;m<ogrenciler.length;m++) {
			System.out.println(ogrenciler[m]);
		}
		//Yeni ekleme için yapıldığında eleman sayısını da arttırmak unutulmamalı (new String [4])

		// Daha çok aşağıdaki şekilde kullanılır, isimlendirmede açık açık yazmaya özen gösterilmelidir.
		System.out.println("En sık kullanılan ve çok daha iyi bir yöntemle yazılan: ");

		for(String ogrenci : ogrenciler) {
			System.out.println(ogrenci);
		}
		
	//------------------------------------Ders 15- ARRAYS (DİZİLER) ve ReCap------------------------------------------

		System.out.println("-----Ders 15- ARRAYS (DİZİLER) ve ReCap");
		
		double[] myList = {1.2,3.4,5.6};
		double total= 0;
		double max= myList[0];
		
		for (double number: myList) {
			//Sayıların toplamını görmek için;
			total= total + number;
			//Sayıların içerisindeki en büyüğü görmek için;
			if(max<number) {
				max=number;
			}
			System.out.println(number);
			System.out.println("Toplam = "+total);
			System.out.println("En Büyük ="+max);
		}
		
	//------------------------------------Ders 16- MULTIDIMENSIONAL ARRAYS (ÇOK BOYUTLU DİZİLER)------------------------------------------
	//Matrisler gibi
		
		System.out.println("-----Ders 16- MULTIDIMENSIONAL ARRAYS (ÇOK BOYUTLU DİZİLER)");
		
		String[][] sehirler = new String[3][3];
		
		sehirler[0][0]= "İstanbul";
		sehirler[0][1]= "Bursa";
		sehirler[0][2]= "Bilecik";
		sehirler[1][0]= "Ankara";
		sehirler[1][1]= "Konya";
		sehirler[1][2]= "Kayseri";
		sehirler[2][0]= "Diyarbakır";
		sehirler[2][1]= "Şanlıurfa";
		sehirler[2][2]= "Gaziantep";
		
		//Nested loop - içiçe döngüler:
		for(int a= 0; a<=2; a++) {
			System.out.println("-----");
			for(int b=0; b<=2; b++) {
				System.out.println(sehirler[a][b]);
			}
		}
		
	//------------------------------------Ders 17- STRINGS------------------------------------------

		System.out.println("-----Ders 17- STRINGS");
		
		String mesaj2 = "Bugün hava çok güzel.";
		// char[] mesaj2 = String mesaj2
			
		System.out.println(mesaj2);
		System.out.println("Eleman sayısı: "+mesaj2.length());
		System.out.println("5. eleman: "+mesaj2.charAt(4)); 
		System.out.println(mesaj2.concat(" Yaşasın!"));
		//concat ile yeni bir string oluşturmuş olduk, önceki mesaj stringini değiştirmedik. 
		//Tekrar mesaj bu haliyle yazdırılmak istense bu halini yeniden tanımlamak gerekir.
		System.out.println(mesaj2.startsWith("A"));
		System.out.println(mesaj2.endsWith("l"));
		//Yukarıdakiler boolean fonksiyonlarıdır, t/f çalıştırır. Case sensitivity'e dikkat edilimesi gerekir.
		char[] karakterler = new char[5];
		mesaj2.getChars(0, 5, karakterler, 0);
		System.out.println(karakterler);
		//Daha yoğun kullanılan bir başka şekil:
		System.out.println(mesaj2.indexOf('a')); //aramaya soldan başlar, numaralandırma 0 ile başlar.
		System.out.println(mesaj2.lastIndexOf("a")); //aramaya sağdan başlar, baştan numaralandırır.
		
	//------------------------------------Ders 18- STRINGS Devam------------------------------------------

		System.out.println("-----Ders 18- STRINGS Devam");
		
		System.out.println(mesaj2.replace(' ', '-')); //metni değiştirmez, yeni veri oluşturur sadece
		
		//Ya da bunun yerine mesaj komple değiştirilmek istenirse:
		String yeniMesaj = mesaj2.replace(' ', '/');
		System.out.println(yeniMesaj);
		
		System.out.println(mesaj2.substring(2)); //0'dan başlayacağı unutulmamalı, arada bir parça almak için:
		System.out.println(mesaj2.substring(2,8)); //8. eleman dahil edilmeyecektir.
		
		//bir eleman aratıp o eleman sayesinde kestirmek için (ör. kelimeleri ayırmak için)
		for(String kelime: mesaj2.split(" ")) {
			System.out.println(kelime);
		};
		
		//Büyük-küçük harf dönüşümü, mesela bir arama yapılacağında büyük-küçük harf duyarlılığını ortadan kaldırmak için kullanılabilir.
		System.out.println(mesaj2.toLowerCase());
		System.out.println(mesaj2.toUpperCase());
		
		System.out.println(mesaj2.trim()); //baştaki sondaki boşlukları temizlemek için
		
	//------------------------------------Ders 19- MİNİ PROJE 1 / ASAL SAYILAR------------------------------------------

		System.out.println("-----Ders 19- MİNİ PROJE 1 / ASAL SAYILAR");
		
		int number2 = 2;
		boolean isPrime = true;
		
		if(number2<2) {
			System.out.println("Geçersiz sayı");
			return; //sistemin burada bitirilmesini sağlar, alttaki kodlar geçersizleşir.
			}
		for (int divider=2; divider<number2; divider++) {
			if(number2 % divider == 0) {
				isPrime = false;
				}
			}
		//int remainder = number %2; 2'ye bölümünden kalan = remainder.
		if(isPrime) {
			System.out.println("Sayı asaldır.");
		}else {
			System.out.println("Sayı asal değildir.");
		}
		
	//------------------------------------Ders 20- MİNİ PROJE 2 / KALIN-İNCE SESLER------------------------------------------

		System.out.println("-----Ders 20- MİNİ PROJE 2 / KALIN-İNCE SESLER");
		
		char harf= 'a';
		switch (harf) {
			case 'A':
			case 'I':
			case 'O':
			case 'U':
				System.out.println("Kalın sesli harftir.");
				break;
			case 'E':
			case 'İ':
			case 'Ö':
			case 'Ü':
				System.out.println("İnce sesli harftir.");
				default:
					System.out.println("Sesli harf değildir.");
		}
		// PEKİİ KÜÇÜK HARFLE YAZILMIŞ OLSAYDI??? harf.toUpperCase char için hata veriyor.
		
	//------------------------------------Ders 21- MİNİ PROJE 3 / MÜKEMMEL SAYILAR------------------------------------------
	
		System.out.println("-----Ders 21- MİNİ PROJE 3 / MÜKEMMEL SAYILAR");
		
		//Mükemmel Sayı: Kendinden başka pozitif tüm tam bölenlerinin toplamı kendisine eşit olan sayılar.
		
		int number3 = 28;
		int total2 = 0;
		for(int divider2=1; divider2<number3; divider2++) {
			if(number3 %divider2 ==0 ) {
				total2= total2+ divider2;
			}
		}
		
		if (total2==number3) {
			System.out.println("Mükemmel sayıdır.");
		}else {
			System.out.println("Mükemmel sayı değildir.");
		}
		
		
	//------------------------------------Ders 22- MİNİ PROJE 3 / ARKADAŞ SAYILAR------------------------------------------

		System.out.println("-----Ders 22- MİNİ PROJE 3 / ARKADAŞ SAYILAR");
		
		//Arkadaş Sayı: Kendileri hariç pozitif tam bölen sayıları toplamı birbirlerine eşit olan sayılardır. ör. 220 ve 284.
		
		// && = and , || = or
		
		int number4 = 220;
		int number5 = 284;
		int total3 = 0;
		int total4 = 0;
		
		for(int d=1; d<number4; d++) {
			if(number4%d==0) {
			total3 = total3 + d;
			}
		}
		
		for(int e=1; e<number5; e++) {
			if(number5%e==0) {
			total4 = total4 + e;
			}
		}
		
		if(total3==number5 && total4==number4) {
			System.out.println("Sayılar arkadaştır.");
			}else {
				System.out.println("Sayılar arkadaş değildir.");
			}
		
	//------------------------------------Ders 23- MİNİ PROJE 4 / SAYI BULMA------------------------------------------
		
		System.out.println("-----Ders 23- MİNİ PROJE 4 / SAYI BULMA");
		
		int[] numbers = new int[] {1,2,5,7,9};
		int toFind = 5;
		boolean found = false;
		
		for(int f : numbers) {
			if (f == toFind) {
				found = true;
			}
		}
		
		if(found) {
			System.out.println("Sayı mevcuttur.");
		}else {
			System.out.println("Sayı mevcut değildir.");
		}
				
	}
}
